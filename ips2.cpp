
#include <iostream>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sstream>
void *Leertxt (void *param) {
  char *str;
  char c[2];
  int cnt = 0;

  str = (char *) param;

  int stream = open ("ip.txt", O_RDONLY);

  if (stream == -1) {
    std::cout << "Error al abrir archivo" << std::endl;
    pthread_exit(0);
  }
      system ("ping  -c1 8.8.4.4");
  while (read(stream,c,1)) {
    if (strchr(str,c[0])) {
      cnt++;
      std::string t = "Encontró: " + (std::string ) str + "\n";
      std::cout << t;
      system ("ping  -c1 8.8.4.4");
      
      sleep(1);
    }
  }
 
}
using namespace std;
class ips{
    public:

        ips();
        ips(string Ip, int Transmitidos, int Recibidos, int Perdidos, string Estado);
        

        void setIp(string Ip);
        void setTransmitidos(int Transmitidos);
        void setRecibidos(int Recibidos);
        void setPerdidos(int Perdidos);
        void setEstado(string Estado);
    

        void print();

    private:
        string Ip;
        int Transmitidos;
        int Recibidos;
        int Perdidos;
        string Estado;
};


ips::ips() {
    Ip = "";
    Transmitidos = 0;
    Recibidos = 0;
    Perdidos= 0;
    Estado= "";
    
}

ips::ips(string Ip, int Transmitidos, int Recibidos, int Perdidos, string Estado) {
    this->Ip = Ip;
    this->Transmitidos = Transmitidos;
    this->Recibidos = Recibidos;
    this->Perdidos = Perdidos;
    this->Estado = Estado;
}

//
void ips::setIp(string Ip) {
    this->Ip = Ip;
}

void ips::setTransmitidos(int Transmitidos) {
    this->Transmitidos = Transmitidos;
}

void ips::setRecibidos(int Recibidos) {
    this->Recibidos = Recibidos;
}
void ips::setPerdidos(int Perdidos) {
    this->Perdidos = Perdidos;
}

void ips::setEstado(string Estado) {
    this->Estado = Estado;
}
//
void ips::print() {
    cout << "IP: " << this->Ip << " Transmitidos: " \
    << this->Transmitidos << " Recibidos: " << this->Recibidos << endl;
}




int main(int argc, char *argv[]) 
{
      system ("ping  -c1 8.8.4.4");
  int i = 0;
	
  pthread_t threads[argc - 1];
for (i=0; i < argc - 1; i++) {
    pthread_create(&threads[i], NULL, Leertxt, argv[i+1]);  //tantas hebras como ping querramos hacer
  }


  for (i=0; i< argc - 1; i++) {
    pthread_join(threads[i], NULL);
  }
return 0;
}
